#ifndef _CODELS_H
#define _CODELS_H

#include "detect_object_c_types.h"
#include <visp3/core/vpImage.h>
#include <iostream>

struct detect_object_vpImage
{
    double timestamp;
    vpImage<vpRGBa> I;
};

struct detect_object_vpBBox
{
    std::vector<vpRect> bbox;
};

struct detect_object_vpHomogeneousMatrix
{
    vpHomogeneousMatrix mat;
};

#endif