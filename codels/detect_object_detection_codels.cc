#include <iostream>
#include "acdetect_object.h"
#include "detect_object_c_types.h"

#include <visp3/gui/vpDisplayX.h>
#include <visp3/detection/vpDetectorDNN.h>
#include "codels.h"

/* --- Task detection --------------------------------------------------- */
double tmp_sec, tmp_nsec;

std::string model;
std::string config;

std::vector<vpRect> boundingBoxes;
std::vector<std::vector<vpImagePoint>> objects_polygon;
vpImagePoint center;

int inputWidth = 300, inputHeight = 300;
bool swapRB = false;
float confThresh = 0.5f;
float nmsThresh = 0.4f;

std::string labelFile = "labels.txt";
std::vector<std::string> labels;

vpDetectorDNN dnn;

std::vector<int> classIds;
std::vector<float> confidences;

detect_object_objects *obj;

/** Codel detection_start_codel of task detection.
 *
 * Triggered by detect_object_start.
 * Yields to detect_object_pause_start, detect_object_main.
 */
genom_event
detection_start_codel(const char *model_path, const char *config_path,
                      double meanR, double meanG, double meanB,
                      double scale,
                      const detect_object_vpImage *current_image,
                      bool *detection_enabled,
                      detect_object_vpBBox **bounding_boxes,
                      detect_object_objects *objects_parameters,
                      const detect_object_port_objects *port_objects,
                      const genom_context self)
{
  *detection_enabled = false;
  if(model_path != NULL)
  {
    model = std::string(model_path);
  }
  if(config_path != NULL)
  {
    config = std::string(config_path);
  }

  if(model_path == NULL || config_path == NULL) // should wait.
    return detect_object_pause_start;
  else
  {
    std::cout << meanR << " " << meanB << " " << meanG << std::endl;
    dnn.readNet(model, config);
    dnn.setInputSize(inputWidth, inputHeight);
    dnn.setMean(meanR, meanG, meanB);
    dnn.setScaleFactor(scale);
    dnn.setSwapRB(swapRB);
    dnn.setConfidenceThreshold(confThresh);
    dnn.setNMSThreshold(nmsThresh);

    if (!labelFile.empty()) {
      std::ifstream f_label(labelFile);
      std::string line;
      while (std::getline(f_label, line)) {
        labels.push_back(line);
      }
    }

    *bounding_boxes = new detect_object_vpBBox;

    // IDS
    //
    objects_parameters->_buffer = NULL;
    objects_parameters->_length = 0;
    //

    // OUTPORT
    //
    obj = port_objects->data(self);
    obj->_buffer = NULL;
    obj->_length = 0;

    if(port_objects->write(self))
      std::cout << "Error" << std::endl;
    //

    return detect_object_main;
  }
}


/** Codel detection_main_codel of task detection.
 *
 * Triggered by detect_object_main.
 * Yields to detect_object_pause_main.
 */
genom_event
detection_main_codel(bool detection_enabled,
                     const detect_object_vpImage *current_image,
                     detect_object_vpBBox **bounding_boxes,
                     detect_object_objects *objects_parameters,
                     const detect_object_port_objects *port_objects,
                     const genom_context self)
{
  if(detection_enabled)
  {
    dnn.detect(current_image->I, (*bounding_boxes)->bbox);
    classIds = dnn.getDetectionClassIds();
    confidences = dnn.getDetectionConfidence();

    if((*bounding_boxes)->bbox.size() == 0) // No objects detected in this iteration, release the buffer of objects_parameters.
    {
      if(objects_parameters->_length != 0)
      {
        // IDS
        //
        objects_parameters->_maximum = 0;
        objects_parameters->_length = 0;
        delete [] objects_parameters->_buffer;
        objects_parameters->_buffer = NULL;
        //

        *obj = *objects_parameters; // Empty output port.

        if(port_objects->write(self))
          std::cout << "Error" << std::endl;
      }
    }

    else // Object(s) detected.
    {
      if(objects_parameters->_length != 0) // Releasing already existing buffer.
      {
        delete [] objects_parameters->_buffer;
        objects_parameters->_buffer = NULL;
      }

      objects_parameters->_maximum = (*bounding_boxes)->bbox.size();
      objects_parameters->_length = 0;

      objects_polygon = dnn.getPolygon(); // Get all objects corners. (Same as boundingboxes)

      // Allocating memory for the sequence of detected objects.
      if(objects_parameters->_buffer == NULL)
      {
        objects_parameters->_buffer = new detected_object_params[objects_parameters->_maximum];
      }

      // Filling objects_parameters data structure.
      for(int i = 0; i < (*bounding_boxes)->bbox.size(); i++)
      {
        tmp_sec    = current_image->timestamp / 1000;
        tmp_nsec = ((long)current_image->timestamp % 1000) * 1000000;

        // Save timestamp of image as timestamp of detected object.
        objects_parameters->_buffer[i].ts.sec  = static_cast<int32_t>(tmp_sec);
        objects_parameters->_buffer[i].ts.nsec = static_cast<int32_t>(tmp_nsec);

        // Save class ID of detected object.
        objects_parameters->_buffer[i].class_id = classIds[i];

        // Save center of object.
        center = dnn.getCog(i);
        objects_parameters->_buffer[i].center._present = true;
        objects_parameters->_buffer[i].center._value.u = center.get_i();
        objects_parameters->_buffer[i].center._value.v = center.get_j();

        // Save corners.
        objects_parameters->_buffer[i].uv_corners_pos._present = true;
        for(int j = 0; j < 4; j++)
        {
          objects_parameters->_buffer[i].uv_corners_pos._value[j].u = objects_polygon[i][j].get_i();
          objects_parameters->_buffer[i].uv_corners_pos._value[j].v = objects_polygon[i][j].get_j();
        }

        // // Save detected object's pose.
        // cMo_vec[i].extract(cto);
        // cMo_vec[i].extract(cqo);

        // objects_parameters->_buffer[i].pos._present = true;
        // objects_parameters->_buffer[i].pos._value.x = cto[0];
        // objects_parameters->_buffer[i].pos._value.y = cto[1];
        // objects_parameters->_buffer[i].pos._value.z = cto[2];

        // objects_parameters->_buffer[i].att._present = true;
        // objects_parameters->_buffer[i].att._value.qx = cqo[0];
        // objects_parameters->_buffer[i].att._value.qy = cqo[1];
        // objects_parameters->_buffer[i].att._value.qz = cqo[2];
        // objects_parameters->_buffer[i].att._value.qw = cqo[3];

        // Save object's class name.
        objects_parameters->_buffer[i].class_name._present = true;
        if(!labels.empty())
          strcpy(objects_parameters->_buffer[i].class_name._value, labels[classIds[i]].c_str());

        objects_parameters->_length++;
      }
    }

    // OUTPORT
    //
    obj = port_objects->data(self); // Is it necessary to read ?
    *obj = *objects_parameters;

    if(port_objects->write(self))
      std::cout << "Error" << std::endl;
    //
  }

  else // In case disabling detection, should update port.
  {
    (*bounding_boxes)->bbox.clear();
    if(objects_parameters->_length != 0)
      {
        // IDS
        //
        objects_parameters->_maximum = 0;
        objects_parameters->_length = 0;
        delete [] objects_parameters->_buffer;
        objects_parameters->_buffer = NULL;
        //

        *obj = *objects_parameters; // Empty output port.

        if(port_objects->write(self))
          std::cout << "Error" << std::endl;
      }
  }

  return detect_object_pause_main;
}


/** Codel detection_stop_codel of task detection.
 *
 * Triggered by detect_object_stop.
 * Yields to detect_object_ether.
 */
genom_event
detection_stop_codel(detect_object_vpBBox **bounding_boxes,
                     detect_object_objects *objects_parameters,
                     const genom_context self)
{
  if(*bounding_boxes != NULL)
    delete *bounding_boxes;

  if(objects_parameters->_buffer != NULL && objects_parameters->_length != 0) // Releasing already existing buffer.
  {
    delete [] objects_parameters->_buffer;
    objects_parameters->_buffer = NULL;
  }

  return detect_object_ether;
}
