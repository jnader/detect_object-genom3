
#include "acdetect_object.h"

#include "detect_object_c_types.h"
#include "codels.h"
#include <visp3/gui/vpDisplayX.h>
#include <visp3/core/vpPoint.h>
#include <visp3/detection/vpDetectorDNN.h>
#include <visp3/core/vpMeterPixelConversion.h>
#include <visp3/core/vpPixelMeterConversion.h>

/* --- Task display ----------------------------------------------------- */
vpDisplayX d;
vpMouseButton::vpMouseButtonType clicked_button;
vpImagePoint display_center, draw_center;
vpCameraParameters display_controller_cam;
vpRect desired_bbox;

/** Codel display_start_codel of task display.
 *
 * Triggered by detect_object_start.
 * Yields to detect_object_pause_start, detect_object_main.
 */
genom_event
display_start_codel(bool display_enabled,
                    const detect_object_vpImage *current_image,
                    const detect_object_ids_camera *cam,
                    double object_width, double object_height,
                    const genom_context self)
{
  if(current_image->I.getSize() != 0 && display_enabled)
  {
    d.init(const_cast<vpImage<vpRGBa>&>(current_image->I));
    display_controller_cam = vpCameraParameters(cam->px, cam->py, cam->u0, cam->v0, cam->kud, cam->kdu); // for my pc.

    double X[4] = {object_width / 2, object_width / 2, -object_width / 2, -object_width / 2};
    double Y[4] = {object_height / 2, -object_height / 2, -object_height / 2, object_height / 2};

    vpHomogeneousMatrix cdMo(0, 0, .75, 0, 0, 0);
    vpPoint P_d;
    vpImagePoint top_left, bottom_right;

    P_d = vpPoint(X[0], Y[0], 0);
    P_d.track(cdMo);
    vpMeterPixelConversion::convertPoint(display_controller_cam, P_d.get_x(), P_d.get_y(), bottom_right);

    P_d = vpPoint(X[2], Y[2], 0);
    P_d.track(cdMo);
    vpMeterPixelConversion::convertPoint(display_controller_cam, P_d.get_x(), P_d.get_y(), top_left);
    desired_bbox.set(top_left, bottom_right);

    return detect_object_main;
  }

  return detect_object_pause_start;
}


/** Codel display_main_codel of task display.
 *
 * Triggered by detect_object_main.
 * Yields to detect_object_pause_main, detect_object_stop.
 */
genom_event
display_main_codel(bool display_enabled,
                   const detect_object_vpImage *current_image,
                   const detect_object_objects *objects_parameters,
                   const detect_object_vpBBox *bounding_boxes,
                   const genom_context self)
{
  if(display_enabled)
  {
    if(current_image != NULL) // To make sure it doesn't access current_image after destruction.
      vpDisplay::display(current_image->I);

    if(bounding_boxes != NULL)
    {
      for (size_t i = 0; i < bounding_boxes->bbox.size(); i++)
        vpDisplay::displayRectangle(current_image->I, bounding_boxes->bbox[i], vpColor::red, false, 2);
        vpDisplay::displayRectangle(current_image->I, desired_bbox, vpColor::green, false, 2);

      if(bounding_boxes->bbox.size() == 1)
      {
        display_center = bounding_boxes->bbox[0].getCenter();
        vpDisplay::displayCircle(current_image->I, display_center, 3, vpColor::red, true, 1);
      }
    }

    if(vpDisplay::getClick(current_image->I, clicked_button, false))
    {
      if(clicked_button == vpMouseButton::vpMouseButtonType::button3) // right-click
        return detect_object_stop;
    }

    vpDisplay::flush(current_image->I);
  }
  return detect_object_pause_main;
}


/** Codel display_stop_codel of task display.
 *
 * Triggered by detect_object_stop.
 * Yields to detect_object_ether.
 */
genom_event
display_stop_codel(const genom_context self)
{
  if(d.isInitialised())
    d.~vpDisplayX();
  
  // if(*desired_bounding_box != NULL)
  // {
  //   delete *desired_bounding_box;
  //   *desired_bounding_box = NULL;
  // }

  return detect_object_ether;
}
