#include "acdetect_object.h"

#include "detect_object_c_types.h"
#include "codels.h"

/* --- Function enable_detection ---------------------------------------- */

/** Codel enable_detection_codel of function enable_detection.
 *
 * Returns genom_ok.
 */
genom_event
enable_detection_codel(bool *detection_enabled,
                       const genom_context self)
{
  *detection_enabled = true;
  return genom_ok;
}


/* --- Function enable_tracking ----------------------------------------- */

/** Codel enable_tracking_codel of function enable_tracking.
 *
 * Returns genom_ok.
 */
genom_event
enable_tracking_codel(bool *tracking_enabled,
                      bool *first_time_tracking_enabled,
                      const genom_context self)
{
  *tracking_enabled = true;
  *first_time_tracking_enabled = true;
  return genom_ok;
}


/* --- Function enable_display ------------------------------------------ */

/** Codel enable_display_codel of function enable_display.
 *
 * Returns genom_ok.
 */
genom_event
enable_display_codel(bool *display_enabled, const genom_context self)
{
  *display_enabled = true;
  return genom_ok;
}


/* --- Function disable_detection --------------------------------------- */

/** Codel disable_detection_codel of function disable_detection.
 *
 * Returns genom_ok.
 */
genom_event
disable_detection_codel(bool *detection_enabled,
                        const genom_context self)
{
  *detection_enabled = false;
  return genom_ok;
}


/* --- Function disable_display ----------------------------------------- */

/** Codel disable_display_codel of function disable_display.
 *
 * Returns genom_ok.
 */
genom_event
disable_display_codel(bool *display_enabled, const genom_context self)
{
  *display_enabled = false;
  return genom_ok;
}


/* --- Function disable_tracking ---------------------------------------- */

/** Codel disable_tracking_codel of function disable_tracking.
 *
 * Returns genom_ok.
 */
genom_event
disable_tracking_codel(bool *tracking_enabled,
                       const genom_context self)
{
  *tracking_enabled = false;
  return genom_ok;
}


/* --- Function set_cMe ------------------------------------------------- */

/** Codel set_cMe_codel of function set_cMe.
 *
 * Returns genom_ok.
 */
genom_event
set_cMe_codel(const sequence4_sequence4_double *arg_cMe,
              detect_object_vpHomogeneousMatrix **cMe,
              const genom_context self)
{

  if((*cMe) == NULL)
    (*cMe) = new detect_object_vpHomogeneousMatrix;

  for(int i = 0; i < 4; i++)
    for(int j = 0; j < 4; j++)
      (*cMe)->mat[i][j] = arg_cMe->_buffer[i]._buffer[j];

  return genom_ok;
}
