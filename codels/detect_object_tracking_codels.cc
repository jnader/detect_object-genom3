#include "acdetect_object.h"

#include "detect_object_c_types.h"
#include "codels.h"
#include <visp3/core/vpPlane.h>
#include <visp3/core/vpXmlParserCamera.h>
#include <visp3/core/vpPixelMeterConversion.h>
#include <visp3/core/vpMomentObject.h>
#include <visp3/core/vpMomentDatabase.h>
#include <visp3/core/vpMomentBasic.h>
#include <visp3/core/vpMomentGravityCenter.h>
#include <visp3/core/vpMomentCentered.h>
#include <visp3/core/vpMomentAreaNormalized.h>
#include <visp3/core/vpMomentGravityCenterNormalized.h>
#include <visp3/visual_features/vpFeatureMomentGravityCenterNormalized.h>
#include <visp3/visual_features/vpFeatureMomentAreaNormalized.h>
#include <visp3/visual_features/vpFeatureVanishingPoint.h>
#include <visp3/visual_features/vpFeatureBuilder.h>
#include <visp3/vs/vpServo.h>

/* --- Task tracking ---------------------------------------------------- */
or_rigid_body_state *ddata;
bool vec_ip_has_been_sorted = false;
bool first_servo_time = false; // Used to initialize task in the first iteration after init_servo() is called.
double first_servo_ts;
vpServo *t;
double servo_error, convergence_threshold = 0.01;

double area = 0.;
double z_desired = 0.75;

// Desired plane
vpPlane plane;
double A = 0.;
double B = 0.;
double C = 1.;

vpCameraParameters controller_cam;

std::vector<std::pair<size_t, vpImagePoint> > vec_ip_sorted, initial_vec_ip_sorted;
std::vector<vpImagePoint> vec_ip;
std::vector<vpPoint> vec_P;
std::vector<vpPoint> vec_P_d;

vpVelocityTwistMatrix cVe;
vpMatrix eJe = vpMatrix(6, 6, 0);
vpColVector ve = vpColVector(6); // for linear and angular velocities

// Momemnts
vpMomentObject *m_obj = NULL;
vpMomentObject *m_obj_d = NULL;
vpMomentDatabase *mdb = NULL, *mdb_d = NULL;
vpMomentBasic *mb_d = NULL;
vpMomentGravityCenter *mg = NULL, *mg_d = NULL;
vpMomentCentered *mc = NULL, *mc_d = NULL;
vpMomentAreaNormalized *man = NULL;
vpMomentAreaNormalized *man_d = NULL;
vpMomentGravityCenterNormalized *mgn = NULL, *mgn_d = NULL;

// Features
// vpFeatureVanishingPoint *s_vp = NULL, *s_vp_d = NULL;
vpFeatureMomentGravityCenterNormalized *s_mgn = NULL;
vpFeatureMomentGravityCenterNormalized *s_mgn_d = NULL;
vpFeatureMomentAreaNormalized *s_man = NULL;
vpFeatureMomentAreaNormalized *s_man_d = NULL;


/** Codel tracking_start_codel of task tracking.
 *
 * Triggered by detect_object_start.
 * Yields to detect_object_pause_start, detect_object_main.
 */
genom_event
tracking_start_codel(bool *tracking_enabled,
                     const detect_object_desired_cmd *desired_cmd,
                     const genom_context self)
{
  *tracking_enabled = false;

  // Initializing port out 'desired_cmd' by setting all the flags to false except velocities.
  ddata = desired_cmd->data(self);
  ddata->ts.sec = 0;
  ddata->ts.nsec = 0;
  ddata->intrinsic = false;
  ddata->pos._present = false;
  ddata->att._present = false;
  ddata->vel._present = true;
  ddata->avel._present = true;
  ddata->acc._present = false;
  ddata->aacc._present = false;
  ddata->jerk._present = false;
  ddata->snap._present = false;
  desired_cmd->write(self);

  return detect_object_main;
}


/** Codel tracking_main_codel of task tracking.
 *
 * Triggered by detect_object_main.
 * Yields to detect_object_pause_main.
 */
genom_event
tracking_main_codel(bool tracking_enabled, double object_width,
                    double object_height,
                    const detect_object_ids_gain *lambda,
                    const detect_object_vpBBox *bounding_boxes,
                    bool *first_time_tracking_enabled,
                    detect_object_vpBBox **desired_bounding_box,
                    const detect_object_ids_camera *cam,
                    const detect_object_vpHomogeneousMatrix *cMe,
                    const detect_object_desired_cmd *desired_cmd,
                    const genom_context self)
{
  if(tracking_enabled)
  {
    if(*first_time_tracking_enabled)
    {
      if(t != NULL)
        delete t;

      if(m_obj_d != NULL)
        delete m_obj_d;

      if(mdb_d != NULL)
        delete mdb_d;

      if(mb_d != NULL)
        delete mb_d;

      if(s_mgn_d != NULL)
        delete s_mgn_d;

      if(s_man_d != NULL)
        delete s_man_d;

      if(m_obj != NULL)
        delete m_obj;

      if(mdb != NULL)
        delete mdb;

      if(s_mgn != NULL)
        delete s_mgn;

      if(s_man != NULL)
        delete s_man;

      if(mg != NULL)
        delete mg;

      if(mg_d != NULL)
        delete mg_d;

      if(mc != NULL)
        delete mc;

      if(mc_d != NULL)
        delete mc_d;

      if(man != NULL)
        delete man;

      if(man_d != NULL)
        delete man_d;

      if(mgn != NULL)
        delete mgn;

      if(mgn_d != NULL)
        delete mgn_d;

      t = new vpServo;
      mdb_d = new vpMomentDatabase;
      mdb = new vpMomentDatabase;
      mb_d = new vpMomentBasic;

      // t->setServo(vpServo::EYEINHAND_CAMERA);
      t->setServo(vpServo::EYEINHAND_L_cVe_eJe);
      t->setInteractionMatrixType(vpServo::CURRENT);
      t->setLambda(lambda->zero, lambda->inf, lambda->dot_0);

      cVe.buildFrom(cMe->mat);

      eJe[0][0] = 1; eJe[1][1] = 1;
      eJe[2][2] = 1; eJe[3][3] = 0;
      eJe[4][4] = 0; eJe[5][5] = 0;

      // Define camera object.
      controller_cam = vpCameraParameters(cam->px, cam->py, cam->u0, cam->v0, cam->kud, cam->kdu);

      // Oject's bounding box size.
      double X[4] = {object_width / 2, object_width / 2, -object_width / 2, -object_width / 2};
      double Y[4] = {object_height / 2, -object_height / 2, -object_height / 2, object_height / 2};

      if(vec_P_d.size() != 0)
        vec_P_d.clear();
      for (int i = 0; i < 4; i++) {
        double x = 0, y = 0;
        // vpPixelMeterConversion::convertPoint(controller_cam, vec_ip[i], x, y);
        vpPoint P_d(X[i], Y[i], 0);
        vpHomogeneousMatrix cdMo(0, 0, z_desired, 0, 0, 0);
        P_d.track(cdMo);
        vec_P_d.push_back(P_d);
      }

      // reset this vector after loading it with desired image points.
      vec_ip.clear();

      plane.init(vec_P_d[0], vec_P_d[1], vec_P_d[2]);

      A = plane.getA();
      B = plane.getB();
      C = plane.getC();

      // Desired moments
      m_obj_d = new vpMomentObject(3);
      m_obj_d->setType(vpMomentObject::DISCRETE); // Consider the object as a polygon
      m_obj_d->fromVector(vec_P_d);                    // Initialize the object with the points coordinates

      s_mgn_d = new vpFeatureMomentGravityCenterNormalized(*mdb_d, A, B, C);
      s_mgn = new vpFeatureMomentGravityCenterNormalized(*mdb, A, B, C);
      s_man_d = new vpFeatureMomentAreaNormalized(*mdb_d, A, B, C);
      s_man = new vpFeatureMomentAreaNormalized(*mdb, A, B, C);

      mg = new vpMomentGravityCenter;
      mg_d = new vpMomentGravityCenter;
      mc = new vpMomentCentered;
      mc_d = new vpMomentCentered;
      man = new vpMomentAreaNormalized(0,0);
      man_d = new vpMomentAreaNormalized(0,0);
      mgn = new vpMomentGravityCenterNormalized;
      mgn_d = new vpMomentGravityCenterNormalized;

      man->setDesiredDepth(z_desired);
      man_d->setDesiredDepth(z_desired);

      m_obj = new vpMomentObject(3);

      mb_d->linkTo(*mdb_d);       // Add basic moments to database
      mg_d->linkTo(*mdb_d);       // Add gravity center to database
      mc_d->linkTo(*mdb_d);       // Add centered moments to database
      man_d->linkTo(*mdb_d);      // Add area normalized to database
      mgn_d->linkTo(*mdb_d);      // Add gravity center normalized to database
      mdb_d->updateAll(*m_obj_d); // All of the moments must be updated, not just an_d
      mg_d->compute();                    // Compute gravity center moment
      mc_d->compute();                    // Compute centered moments AFTER gravity center

      if (m_obj_d->getType() == vpMomentObject::DISCRETE)
        area = mb_d->get(2, 0) + mb_d->get(0, 2);
      else
        area = mb_d->get(0, 0);

      // Update moment with the desired area
      man_d->setDesiredArea(area);

      man_d->compute(); // Compute area normalized moment AFTER centered moments
      mgn_d->compute(); // Compute gravity center normalized moment AFTER area normalized moment

      // Add the features
      t->addFeature(*s_mgn, *s_mgn_d);
      t->addFeature(*s_man, *s_man_d);

      // Update desired gravity center normalized feature
      s_mgn_d->update(A, B, C);
      s_mgn_d->compute_interaction();
      // Update desired area normalized feature
      s_man_d->update(A, B, C);
      s_man_d->compute_interaction();

      std::cout << "Visual servoing task initialized" << std::endl;
      first_servo_ts = vpTime::measureTimeSecond();
      *first_time_tracking_enabled = false;
      std::cout << "Reset servo time." << std::endl;
    }

    if(bounding_boxes->bbox.size() == 1) // Only 1 object is detected => calculate command.
    {
      vec_P.clear();
      // Update current points used to compute moments. Taking only first detected tag ([0]).
      vec_ip.push_back(bounding_boxes->bbox[0].getTopRight());
      vec_ip.push_back(bounding_boxes->bbox[0].getBottomRight());
      vec_ip.push_back(bounding_boxes->bbox[0].getBottomLeft());
      vec_ip.push_back(bounding_boxes->bbox[0].getTopLeft());
      vec_ip.push_back(bounding_boxes->bbox[0].getTopRight());
      for(int i = 0; i < 5; i++)
      {
        // The points are arriving in CCW not CW. This is why we have to read
        // T265-genom3 tag corners from the end and store them in the beginning.
        // vec_ip.push_back(bounding_boxes->bbox[0]);
        double x = 0, y = 0;
        vpPixelMeterConversion::convertPoint(controller_cam, vec_ip[i], x, y);
        vpPoint P;
        P.set_x(x);
        P.set_y(y);
        vec_P.push_back(P);
      }

      m_obj->setType(vpMomentObject::DISCRETE); // Consider the object as a polygon
      m_obj->fromVector(vec_P); // Initialize the object with the points coordinates

      mg->linkTo(*mdb);       // Add gravity center to database
      mc->linkTo(*mdb);       // Add centered moments to database
      man->linkTo(*mdb);      // Add area normalized to database
      mgn->linkTo(*mdb);      // Add gravity center normalized to database
      mdb->updateAll(*m_obj); // All of the moments must be updated, not just an_d
      mg->compute();         // Compute gravity center moment
      mc->compute();         // Compute centered moments AFTER gravity center

      man->setDesiredArea(area); // Desired area was init at 0 (unknow at contruction), need to be updated here
      man->compute();            // Compute area normalized moment AFTER centered moment
      mgn->compute();            // Compute gravity center normalized moment AFTER area normalized moment
      s_mgn->update(A, B, C);
      s_mgn->compute_interaction();
      s_man->update(A, B, C);
      s_man->compute_interaction();

      t->set_cVe(cVe);
      t->set_eJe(eJe);

      // Compute the control law. Velocities are computed in the mobile robot reference frame.
      ve = t->computeControlLaw(vpTime::measureTimeSecond() - first_servo_ts);
      // Calculating error.
      servo_error = t->getError().sumSquare();
      // std::cout << servo_error << std::endl;
      // if(servo_error < convergence_threshold)
      //   *servo_converged = true;

      // (*nb_detections)++;

      vec_ip.clear();
      // std::cout << ve.t() << std::endl;
    }

    else // No or > 1 objects detected => set command = 0
      ve = 0.;
  }

  else // Tracking disabled.
    ve = 0.;

  // Copy ve to cmd
  ddata = desired_cmd->data(self);
  ddata->intrinsic = true;
  ddata->vel._present = true;
  ddata->vel._value.vx = ve[0];
  ddata->vel._value.vy = ve[1];
  ddata->vel._value.vz = ve[2];
  ddata->avel._present = true;
  ddata->avel._value.wx = ve[3];
  ddata->avel._value.wy = ve[4];
  ddata->avel._value.wz = ve[5];
  if(desired_cmd->write(self))
    std::cout << "Can't write to desired command port." << std::endl;;

  std::cout << ve.t() << std::endl;

  return detect_object_pause_main;
}


/** Codel tracking_stop_codel of task tracking.
 *
 * Triggered by detect_object_stop.
 * Yields to detect_object_ether.
 */
genom_event
tracking_stop_codel(const detect_object_desired_cmd *desired_cmd,
                    const genom_context self)
{
  /* skeleton sample: insert your code */
  /* skeleton sample */ return detect_object_ether;
}
