#include "acdetect_object.h"

#include "detect_object_c_types.h"
// #include <visp3/gui/vpDisplayX.h>

#include "codels.h"
#include <iostream>
#include <visp3/sensor/vpV4l2Grabber.h>

/* --- Task grabber ----------------------------------------------------- */

double timestamp;
vpV4l2Grabber g;

/** Codel grabber_start_codel of task grabber.
 *
 * Triggered by detect_object_start.
 * Yields to detect_object_pause_start, detect_object_main.
 */
genom_event
grabber_start_codel(detect_object_vpImage **current_image,
                    const genom_context self)
{
  g = vpV4l2Grabber(); // Set it to default settings: /dev/video0, ...

  // Since the grabber will now be created, allocate memory for the image.
  if(*current_image == NULL) // just in case.
  {
    *current_image = new detect_object_vpImage;
    (*current_image)->I = vpImage<vpRGBa>();
  }

  // should be more specific in this case. If /dev/video1 for example, ...
  g.setHeight(480);
  g.setWidth(640);
  g.open((*current_image)->I);

  return detect_object_main;
}

/** Codel grabber_main_codel of task grabber.
 *
 * Triggered by detect_object_main.
 * Yields to detect_object_pause_main.
 */
genom_event
grabber_main_codel(detect_object_vpImage **current_image,
                   const genom_context self)
{
  g.acquire((*current_image)->I);
  timestamp = vpTime::measureTimeSecond();

  (*current_image)->timestamp = timestamp;

  return detect_object_pause_main;
}


/** Codel grabber_stop_codel of task grabber.
 *
 * Triggered by detect_object_stop.
 * Yields to detect_object_ether.
 */
genom_event
grabber_stop_codel(detect_object_vpImage **current_image,
                   const genom_context self)
{
  if(*current_image != NULL)
    delete *current_image;

  g.close();

  return detect_object_ether;
}
