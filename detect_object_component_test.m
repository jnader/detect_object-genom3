client = genomix.client();

det_obj = client.load('detect_object');

det_obj.set_meanR(127.5);
det_obj.set_meanG(127.5);
det_obj.set_meanB(127.5);
det_obj.set_scale(1);
det_obj.set_object_width(0.15);
det_obj.set_object_height(0.2);

s = struct();
s.cam.px = 662;
s.cam.py = 661;
s.cam.u0 = 325.5;
s.cam.v0 = 225;
s.cam.kud = 0.084;
s.cam.kdu = -0.084;

s.lambda.zero = 0.5;
s.lambda.inf = 0.3;
s.lambda.dot_0 = 30;

A = eye(4);

for i = 1:4
s.arg_cMe{i} = num2cell(A(i,:));
end

det_obj.set_camera_parameters(s);
det_obj.set_gain(s);
det_obj.set_cMe(s);


% det_obj.set_model_path('ssd-mobilenet_5.onnx');
det_obj.set_model_path('/home/jnader/programming-ws/visp-jnader/build/tutorial/detection/dnn/opencv_face_detector_uint8.pb');
% det_obj.set_config_path(' ');
det_obj.set_config_path('/home/jnader/programming-ws/visp-jnader/build/tutorial/detection/dnn/opencv_face_detector.pbtxt');
det_obj.enable_detection();
det_obj.enable_display();


% det_obj.enable_tracking();